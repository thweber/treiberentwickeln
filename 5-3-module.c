/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/init.h>

static int __init mod_init(void)
{
	printk("mod_init called\n");
	return 0;
}

static void __exit mod_exit(void)
{
	printk("mod_exit called\n");
}

module_init( mod_init );
module_exit( mod_exit );

/* Metainformation */
MODULE_AUTHOR("J�rgen Quade");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Just a Module-Template, without specific functionality.");
MODULE_SUPPORTED_DEVICE("none");
