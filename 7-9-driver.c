/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/fs.h>
#include <linux/platform_device.h>

static struct device_driver speaker_driver = {
	.name = "speaker_driver",
	.bus = &platform_bus_type,
};

static int __init mod_init(void)
{
	if( driver_register(&speaker_driver)!=0 )
		return -EIO;
	/*
	 * Ab hier folgt die normale Treiberinitialisierung.
	 * ...
	 * return 0;
         */

free_driver:
	driver_unregister(&speaker_driver);
	return -EIO;
}

static void __exit mod_exit(void)
{
	/*
	 * Deinitialisierung der in mod_init vorgenommenen
	 * Initialisierungen
	 */
	driver_unregister(&speaker_driver);
}

module_init( mod_init );
module_exit( mod_exit );
MODULE_LICENSE("GPL");
