/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
typedef struct _liste {
	struct _liste *next;
	void *element;
} liste;

static liste *root = NULL; 

/* Liste mit globalem Root-Zeiger. */
/* Nicht objektorientiert - nicht mehrfach verwendbar. */
static liste *liste_add( liste *new_element )
{
	liste *lptr;

	if( root == NULL )
		return( NULL );
	for( lptr=root; lptr->next; lptr=lptr->next )
		;
	lptr->next = calloc( 1, sizeof(liste) );
	lptr->next->element = new_element;
	return lptr->next;
}

/* Mehrfach verwendbare Liste (mit objektorientierter Datenhaltung) */
/* Der aufrufspezifische Parameter ist rootptr. Dieser speichert */
/* Informationen �ber die Aufrufe hinweg, f�r jede Instantiierung */
/* (der Liste) jedoch getrennt. */
static liste *liste_add( list *rootptr, liste *new_element )
{
	liste *lptr;

	if( rootptr == NULL )
		return NULL;
	for( lptr=rootptr; lptr->next; lptr=lptr->next )
		;
	lptr->next = calloc( 1, sizeof(liste) );
	lptr->next->element = new_element;
	return lptr->next;
}
