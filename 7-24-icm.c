/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/init.h>
#include <asm/timex.h>
#include <asm/io.h>

static char *textbuf = "Hallo Treiber";

void ton_an( u16 tonwert )
{
	s8 save;

	if( tonwert ) {
		tonwert = CLOCK_TICK_RATE/tonwert;
		printk("ton_an( 0x%x )\n", tonwert );
		outb( 0xb6, 0x43 );
		outb_p(tonwert & 0xff, 0x42);
		outb((tonwert>>8) & 0xff, 0x42);
		save = inb( 0x61 );
		outb( save | 0x03, 0x61 );
	} else {
		outb(inb_p(0x61) & 0xFC, 0x61);
	}
}

static int __init driver_init(void)
{
	printk("%s\n",textbuf);
	return 0;
}

static void __exit driver_exit(void)
{
	outb(inb_p(0x61) & 0xFC, 0x61);
}

module_init( driver_init );
module_exit( driver_exit );
EXPORT_SYMBOL_GPL( ton_an );
EXPORT_SYMBOL_GPL( textbuf );

MODULE_LICENSE("GPL");
