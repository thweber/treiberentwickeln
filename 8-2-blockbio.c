/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/blkdev.h>

#define SIZE_IN_KBYTES 256

static int major;
static struct gendisk *disk;
static struct request_queue *bdqueue;
static char *mempool;
DEFINE_SPINLOCK( bdlock );

static struct block_device_operations bops = {
    .owner           = THIS_MODULE,
};


static void reqfn( struct request_queue *q )
{
	struct request *req;
	char *kaddr, *maddr;
	struct bio_vec *bvec=NULL;
	struct req_iterator iter;

	req = blk_fetch_request(q);
	while( req ) {
		printk("req: %d - ", req->cmd_type );
		if( req->cmd_type != REQ_TYPE_FS ) {
			if( !__blk_end_request_cur(req, 0) )
				req = blk_fetch_request( q );
			continue;
		}
		printk("before rq_for_each_segment()\n");
		rq_for_each_segment( bvec, req, iter ) {
			printk("after rq_for_each_segment(): bvec=%p\n",bvec);
			kaddr = page_address(bvec->bv_page) + bvec->bv_offset;
			printk("page_address: %p\n", kaddr );
			maddr = mempool + (512 * iter.bio->bi_sector);
			printk("kaddr: %p maddr: %p len: %d\n",
				kaddr, maddr, bvec->bv_len );
#if 1
			if( bio_data_dir( iter.bio )==READ ) {
				memcpy( kaddr, maddr, bvec->bv_len );
			} else {
				memcpy( maddr, kaddr, bvec->bv_len );
			}
#endif
		}
		printk("end rq_for_each_segment()\n");
		if( !__blk_end_request_cur(req, 0) )
			req = blk_fetch_request( q );
	}
}

static int __init mod_init(void)
{
	if((major=register_blkdev(0,"bds"))==0) {
		printk("bd: can't get majornumber\n");
		return -EIO;
	}
	if( !(mempool=vmalloc(SIZE_IN_KBYTES*1024)) ) {
		printk("vmalloc failed ...\n");
		goto out_no_mem;
	}
	if( !(disk=alloc_disk(1)) ) {
		printk("alloc_disk failed ...\n");
		goto out;
	}
	disk->major = major;
	disk->first_minor = 0;
	disk->fops = &bops;
	if( (bdqueue=blk_init_queue(reqfn,&bdlock))==NULL ) {
		printk( KERN_ERR "blk_init_queue failed\n" );
		goto out;
	}
	blk_queue_logical_block_size( bdqueue, 512 );
	disk->queue = bdqueue;
	sprintf(disk->disk_name, "bds0");
	set_capacity( disk, (SIZE_IN_KBYTES * 1024)>>9 ); /* in 512 Byte blocks */
	add_disk( disk );
	return 0;
out:
	vfree( mempool );
out_no_mem:
	unregister_blkdev(major, "bds" );
	return -EIO;
}

static void __exit mod_exit(void)
{
	unregister_blkdev(major, "bds" );
	del_gendisk(disk);
	put_disk( disk );
	blk_cleanup_queue( bdqueue ); /* has to be the last statement */
	vfree( mempool );
}

module_init( mod_init );
module_exit( mod_exit );
MODULE_LICENSE("GPL");
