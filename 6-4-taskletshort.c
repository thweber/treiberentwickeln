/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/interrupt.h>

static void tasklet_func( unsigned long data )
{
    printk(KERN_INFO "Tasklet called...\n");
}

DECLARE_TASKLET( tl_descr, tasklet_func, 0L );

static int __init mod_init(void)
{
    printk(KERN_INFO "mod_init called\n");
    tasklet_schedule( &tl_descr ); /* Tasklet sobald als m�glich ausf�hren */
    return 0;
}

static void __exit mod_exit(void)
{
    printk(KERN_INFO "mod_exit called\n");
    tasklet_kill( &tl_descr );
}

module_init( mod_init );
module_exit( mod_exit );

MODULE_LICENSE("GPL");
