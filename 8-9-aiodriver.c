/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/mm.h>
#include <linux/aio.h>
#include <linux/cdev.h>
#include <linux/device.h>

static dev_t async_dev_number;
static struct cdev *driver_object;
struct class *async_class;
static struct device *async_dev;
static wait_queue_head_t read_wq;
static struct timer_list ptimer;
static void *copy_to;
static unsigned int max_copy;

static void timer_function( unsigned long p )
{
	struct kiocb *iocb = (struct kiocb *)p;

	printk("timer_function(%p)\n", iocb );
	if( !kiocbIsCancelled( iocb ) ) {
		printk("starting kick...\n");
		kick_iocb(iocb);
	} else {
		printk("iocb is cancelled ...\n");
	}
	return;
}

static int driver_cancel( struct kiocb *iocb, struct io_event *event )
{
	printk("driver_cancel (%p)...\n", iocb);
	del_timer_sync( &ptimer );
	kiocbSetCancelled( iocb );
	aio_put_req(iocb);
	aio_put_req(iocb);
	return 0;
}

static ssize_t driver_retry( struct kiocb *iocb )
{
	int to_copy, not_copied=0;

	printk("driver_retry ...\n");
	to_copy = (max_copy<12)? max_copy : 12;
	not_copied=copy_to_user(copy_to, "Hello World",to_copy);
	return to_copy-not_copied;
}

static ssize_t driver_aioread(struct kiocb *iocb, const struct iovec *iov,
	size_t count, loff_t offset)
{
	printk( "driver_aioread(%p,%p,%ld,%ld)\n", iocb, iov->iov_base,
		iov->iov_len, count );
	copy_to  = iov->iov_base;
	max_copy = iov->iov_len;
	iocb->ki_cancel = driver_cancel;
	iocb->ki_retry = driver_retry;
	ptimer.function = timer_function;
	ptimer.data = (unsigned long)iocb;
	ptimer.expires = jiffies + (2*HZ);
	add_timer( &ptimer );
	return -EIOCBRETRY;
}

static struct file_operations fops = {
	.owner=THIS_MODULE,
	.aio_read=driver_aioread,
};

static int __init async_init( void )
{
	if( alloc_chrdev_region(&async_dev_number,0,1,"async")<0 )
		return -EIO;
	driver_object = cdev_alloc();
	if( driver_object==NULL )
		goto free_device_number;
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if( cdev_add(driver_object,async_dev_number,1) )
		goto free_cdev;
	async_class = class_create( THIS_MODULE, "async" );
	if( IS_ERR( async_class ) ) {
		pr_err("async: no udev support\n");
		goto free_cdev;
	}
	async_dev = device_create( async_class, NULL,
		async_dev_number, NULL, "%s", "async" );
	init_waitqueue_head( &read_wq );
	init_timer( &ptimer );
	return 0;
free_cdev:
	kobject_put( &driver_object->kobj );
free_device_number:
	unregister_chrdev_region( async_dev_number, 1 );
	return -EIO;
}

static void __exit async_exit( void )
{
	del_timer_sync( &ptimer );
	device_destroy( async_class, async_dev_number );
	class_destroy( async_class );
	cdev_del( driver_object );
	unregister_chrdev_region( async_dev_number, 1 );
	return;
}

module_init( async_init );
module_exit( async_exit );
MODULE_LICENSE("GPL");
