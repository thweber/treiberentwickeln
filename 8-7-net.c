/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/fs.h>
#include <linux/netdevice.h>

static struct net_device *my_net;
static int my_net_probe(struct net_device *dev);

static int my_net_receive( void )
{
	return 0;
}

static int my_net_send(struct sk_buff *skb, struct net_device *dev)
{
	kfree_skb( skb );
	return 0;
}

static int my_net_open(struct net_device *dev)
{
	netif_start_queue(dev);
	return 0;
}

static int my_net_close(struct net_device *dev)
{
	netif_stop_queue(dev);
	return 0;
}

static int my_net_probe(struct net_device *dev)
{
	ether_setup(dev);
	return 0;
}

static const struct net_device_ops mynet = {
	ndo_init: my_net_probe,
	ndo_open: my_net_open,
	ndo_stop: my_net_close,
	ndo_start_xmit: my_net_send,
};

static void my_net_setup( struct net_device *dev )
{
	dev->netdev_ops = &mynet;
	dev->destructor = free_netdev;
}

static int __init net_init(void)
{
	if( (my_net=alloc_netdev(0,"net%d",my_net_setup))==NULL )
			return -ENOMEM;
	return register_netdev(my_net);
}

static void __exit net_exit(void)
{
	unregister_netdev(my_net);
}

module_init( net_init );
module_exit( net_exit );
MODULE_LICENSE("GPL");
