/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/sched.h>

static int thread_id1=0, thread_id2=0;
DECLARE_COMPLETION( cmpltn );
DEFINE_MUTEX( list_mutex );

static int thread_code( void *data )
{
	unsigned long timeout;
	int i;
	wait_queue_head_t wq;

	daemonize("Thread-Test");
	init_waitqueue_head(&wq);
	allow_signal( SIGTERM ); 
	for( i=0; i<20; i++ ) {
		timeout=HZ;
		if( mutex_lock_interruptible( &list_mutex )==-EINTR ) {
			pr_info( "Durch Signal unterbrochen!\n");;
			break;
		}
		/* Kritischer Abschnitt */
		mutex_unlock( &list_mutex );
		timeout=wait_event_interruptible_timeout(wq,(timeout==0),
			timeout);
		pr_info("Thread %d aufgewacht\n", current->pid);
		if( timeout==-ERESTARTSYS ) {
			pr_info( "Durch Signal unterbrochen!\n");;
			break;
		}
	}
	complete_and_exit( &cmpltn, 0 );
	return 0;
}

static int __init driver_init(void)
{
	thread_id1=kernel_thread(thread_code, "Thread1", CLONE_KERNEL);
	if( thread_id1 ) {
		thread_id2=kernel_thread(thread_code, "Thread2", CLONE_KERNEL);
		if( thread_id2 )
			return 0;
		/* Init fehlgeschlagen - aufraeumen */
		kill_pid( find_pid_ns(thread_id1,&init_pid_ns), SIGTERM, 1 );
		wait_for_completion( &cmpltn );
	}
	return -EIO;
}

static void __exit driver_exit(void)
{
	kill_pid( find_pid_ns(thread_id1,&init_pid_ns), SIGTERM, 1 );
	kill_pid( find_pid_ns(thread_id2,&init_pid_ns), SIGTERM, 1 );
	wait_for_completion( &cmpltn );
	wait_for_completion( &cmpltn );
}

module_init( driver_init );
module_exit( driver_exit );
MODULE_LICENSE("GPL");
