/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>

static dev_t book_dev_number;
static struct cdev *driver_object;
static struct class *book_class;

static struct file_operations fops = {
	.owner= THIS_MODULE,
};

static int __init mod_init(void)
{
	if( alloc_chrdev_region(&book_dev_number,0,3,"book")<0 )
		return -EIO;
	driver_object = cdev_alloc(); /* Anmeldeobjekt reservieren */
	if( driver_object==NULL )
		goto free_device_number;
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if( cdev_add(driver_object,book_dev_number,3) )
		goto free_cdev;
	/* Eintrag im Sysfs, damit Udev den Geraetedateieintrag erzeugt. */
	book_class = class_create( THIS_MODULE, "book" );
	if( IS_ERR(book_class) ) {
		printk("book: no udev support.\n");
		goto free_cdev;
	}
	device_create( book_class, NULL, book_dev_number+0,
			NULL,"book%d",0);
	device_create( book_class, NULL, book_dev_number+1,
			NULL,"book%d",1);
	device_create( book_class, NULL, book_dev_number+2,
			NULL,"book%d",2);
	return 0;
free_cdev:
	kobject_put( &driver_object->kobj );
free_device_number:
	unregister_chrdev_region( book_dev_number, 3 );
	return -EIO;
}

static void __exit udev_driver_exit(void)
{
	/* Loeschen des Sysfs-Eintrags und damit der Geraetedatei */
	device_destroy( book_class, book_dev_number+0 );
	device_destroy( book_class, book_dev_number+1 );
	device_destroy( book_class, book_dev_number+2 );
	class_destroy( book_class );
	/* Abmelden des Treibers */
	cdev_del( driver_object );
	unregister_chrdev_region( book_dev_number, 3 );
}

module_init( mod_init );
module_exit( udev_driver_exit );
MODULE_LICENSE("GPL");

