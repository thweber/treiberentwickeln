/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/seq_file.h>

static int sf_show(struct seq_file *m, void *v)
{
	struct task_struct *tptr = current;

	seq_printf(m, "Prozess PID: %d\n", tptr->pid );
	seq_printf(m, "     Eltern: %p\n", tptr->parent );
	return 0;
}

static int call_seq_open( struct inode *geraete_datei, struct file *instanz )
{
	return single_open( instanz, sf_show, NULL );
}

static struct file_operations fops = {
	.owner   = THIS_MODULE,
	.open    = call_seq_open,
	.read    = seq_read,
	.llseek  = seq_lseek,
	.release = single_release,
};

static int __init mod_init(void)
{
	static struct proc_dir_entry *procdirentry;

	procdirentry=create_proc_entry( "seq_file_test", 0, NULL );
	if( procdirentry ) {
		procdirentry->proc_fops = &fops;
	}
	return 0;
}

static void __exit mod_exit(void)
{
	remove_proc_entry( "seq_file_test", NULL );
}

module_init( mod_init );
module_exit( mod_exit );
MODULE_LICENSE("GPL");
