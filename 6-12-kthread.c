/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/completion.h>

static int thread_id=0;
static wait_queue_head_t wq;
static DECLARE_COMPLETION( on_exit );

static int thread_code( void *data )
{
	unsigned long timeout;
	int i;

	daemonize("MyKThread");
	allow_signal( SIGTERM ); 
	for( i=0; i<10; i++ ) {
		timeout=HZ; /* wait 1 second */
		timeout=wait_event_interruptible_timeout(
			wq, (timeout==0), timeout);
		printk("thread_code: woke up ...\n");
		if( timeout==-ERESTARTSYS ) {
			printk("got signal, break\n");
			break;
		}
	}
	thread_id = 0;
	complete_and_exit( &on_exit, 0 );
}

static int __init kthread_init(void)
{
	init_waitqueue_head(&wq);
	thread_id=kernel_thread(thread_code, NULL, CLONE_KERNEL );
	if( thread_id==0 )
		return -EIO;
	return 0;
}

static void __exit kthread_exit(void)
{
	if( thread_id )
		kill_pid( find_pid_ns(thread_id,&init_pid_ns), SIGTERM, 1 );
	wait_for_completion( &on_exit );
}

module_init( kthread_init );
module_exit( kthread_exit );

MODULE_LICENSE("GPL");
