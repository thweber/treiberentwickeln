/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <libaio.h>

#define DRIVERTEST 0
#define BUF_SIZE (64*1024)
static char *buf, *buf2;

int main( int argc, char **argv )
{
	io_context_t ctxp = NULL;
	struct iocb iocb, iocb2;
	struct iocb *iocbs[] = { &iocb, &iocb2 };
	struct io_event event[2];
	int fd1, fd2;
	struct timespec ts;
	int result;

	if( io_setup( 1024, &ctxp ) ) {
		perror( "io_setup" );
		return -1;
	}
#if DRIVERTEST
	fd1 = open( "/dev/async", O_RDONLY );
#else
	fd1 = open( "/tmp/foo", O_RDWR|O_DIRECT );
#endif
	fd2 = open( "/tmp/too", O_RDWR|O_DIRECT );
	if( fd1<0 || fd2<0 ) {
		perror("open");
		return -2;
	}
	posix_memalign( (void**)&buf, 512, BUF_SIZE );
	posix_memalign( (void**)&buf2, 512, BUF_SIZE );
	io_prep_pread( &iocb,  fd1, buf, BUF_SIZE, 0 );
	io_prep_pread( &iocb2, fd2, buf2, BUF_SIZE, 0 );
	io_submit( ctxp, 2, iocbs );
	/* ... do something else ... */
	ts.tv_sec = 5;
	ts.tv_nsec= 0;
	result = io_getevents( ctxp, 2, 2, &event[0], &ts );
	printf("result= %d | result= %ld/%ld res2= %ld/%ld\n", result,
		event[0].res, event[0].res2,
		event[1].res, event[1].res2);
#if DRIVERTEST
	printf("Inhalt von buf: %s\n", buf ); /* Test mit "asynctest" */
#endif
	io_destroy( ctxp );
	return 0;
}
