# Makefile
# zum Beispielcode aus dem Buch
# Linux-Treiber entwickeln - Eine systematsiche Einfuehrung
# in die Geraetetreiber- und Kernelprogrammierung.
# 
# Erschienen im dpunkt-Verlag, 2011
#
# Um saemtlichen Beispielcode zu generieren muessen
# Sie nur "make" eintippen. Je nach eingesetzter
# Kernelversion laesst sich unter Umstaenden nicht
# jedes Modul fehlerfrei compilieren. Loeschen Sie das
# problematische File zunaechst aus der Liste und
# rufen Sie dann "make" erneut auf.
#
# Mi 9. Mär 18:08:19 CET 2011
#
# GPL
# (c) 2011, J. Quade, E. Kunst

ifneq ($(KERNELRELEASE),)
obj-m	:= 3-3-kprobe.o\
	5-14-ioctl.o\
	5-15-poll.o\
	5-1-mod1.o\
	5-22-speaker.o\
	5-23-hw.o\
	5-26-pcitemplate.o\
	5-28-drvrtemplate.o\
	5-3-module.o\
	5-5-devicenumber.o\
	5-8-hello.o\
	6-10-hrtasklet.o\
	6-11-kthreadd.o\
	6-12-kthread.o\
	6-13-sema.o\
	6-14-workqexample.o\
	6-15-events.o\
	6-16-mutex.o\
	6-17-prioinv.o\
	6-19-kernellist.o\
	6-21-slab.o\
	6-4-taskletshort.o\
	6-7-ktimersample.o\
	6-9-hrtimer.o\
	7-10-devdrvbind.o\
	7-11-platform.o\
	7-12-devicedriver.o\
	7-13-suspendtest.o\
	7-16-fwtest.o\
	7-17-fwasync.o\
	7-18-param.o\
	7-24-icm.o\
	7-25-stacked.o\
	7-3-procsimple.o\
	7-6-sfi.o\
	7-7-singleseq.o\
	7-8-udev.o\
	7-9-driver.o\
	8-10-aiodirect.o\
	8-1-blockdevice.o\
	8-2-blockbio.o\
	8-3-blockeasy.o\
	8-5-usbcheck.o\
	8-7-net.o\
	8-9-aiodriver.o

else
KDIR	:= /lib/modules/$(shell uname -r)/build
PWD	:= $(shell pwd)

default:
	$(MAKE)	-C $(KDIR)	M=$(PWD) modules
endif

clean:
	rm -f *~ *.o *.ko
	rm -f .built_in.o.cmd built_in.o
	rm -f .*.cmd *.ko *.mod.c
