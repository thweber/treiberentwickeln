/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
/******************************************************************/
/* Das Programm demonstriert eine Möglichkeit sicherzustellen,    */
/* dass nur die Treiberinstanz Daten vom Treiber liest,           */
/* die auch beim select (poll) die Information bekommen hat, dass */
/* Daten zum Lesen vorhanden sind.                                */
/******************************************************************/
#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <asm/io.h>
#include <asm/uaccess.h>   /* copy_to_user()       */

#define DRIVER_MAJOR 240

static wait_queue_head_t read_queue;
static unsigned long data_avail_for_read = 0;
static struct file *reserved_for_proc=NULL;

static int driver_close( struct inode *inode, struct file *instance )
{
	printk("driver_close aufgerufen\n");
	if( reserved_for_proc==instance ) {
		reserved_for_proc=NULL;
		set_bit(0,&data_avail_for_read);
		wake_up_interruptible( &read_queue );
	}
	return 0;
}

static ssize_t driver_read( struct file *instance, char *user, size_t count,
		loff_t *Offset )
{
	int to_copy = count, retval;

	while( !test_and_clear_bit(0,&data_avail_for_read) ) {
		if( reserved_for_proc==instance ) {
			reserved_for_proc=NULL;
			break;
		}
		printk("going to sleep %p...\n", instance );
		retval=wait_event_interruptible(read_queue,data_avail_for_read);
		printk("woke up %p...\n", instance );
		if( retval==-ERESTARTSYS ) {
			printk("got signal, break\n");
			return -EINTR;
		}
	}
	return to_copy;
}

static unsigned int driver_poll(struct file *instance, poll_table *event_list)
{
    unsigned int mask = 0;

	if( test_and_clear_bit(0,&data_avail_for_read) ) {
		reserved_for_proc =instance;
		mask |= POLLIN | POLLRDNORM;
	}
	poll_wait( instance, &read_queue, event_list );
	return mask;
}

static ssize_t driver_write( struct file *instance, const char *User,
		size_t count, loff_t *offset)
{
	int to_copy = count;

	printk("Daten koennen gelesen werden.\n");
	set_bit(0,&data_avail_for_read);
	wake_up_interruptible( &read_queue );
	return to_copy;
}

static struct file_operations fops = {
.owner=   THIS_MODULE,
.read=    driver_read,  /* read */
.write=   driver_write, /* write */
.poll=    driver_poll,  /* poll */
.release= driver_close, /* release */
};

static int __init driver_init(void)
{
	if(register_chrdev(DRIVER_MAJOR, "Polltest", &fops) == 0) {
		init_waitqueue_head( &read_queue );
		return 0;
	};
	printk("Polltest unable to get major %d\n",DRIVER_MAJOR);
	return -EIO;
}

static void __exit driver_exit(void)
{
	unregister_chrdev(DRIVER_MAJOR,"Polltest");
}

module_init( driver_init );
module_exit( driver_exit );

MODULE_LICENSE("GPL");
