/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>

#define TEMPLATE "template"
static dev_t template_dev_number;
static struct cdev *driver_object;
struct class *template_class;
static struct device *template_dev;

#ifdef CONFIG_PM /* Powermanagement */
static int template_suspend(struct device *dev, pm_message_t state)
{
	switch( state.event ) {
	case PM_EVENT_ON:
		dev_dbg(dev,"on...\n"); break;
	case PM_EVENT_FREEZE:
		dev_dbg(dev,"freeze...\n"); break;
	case PM_EVENT_SUSPEND:
		/* Stromsparmodus einschalten
		 * ...
		 */
		dev_dbg(dev,"suspend...\n"); break;
	case PM_EVENT_HIBERNATE:
		/* Stromsparmodus einschalten
		 * ...
		 */
		dev_dbg(dev,"hibernate...\n"); break;
	default:
		dev_dbg(dev,"pm_event: 0x%x\n", state.event);
		break;
	}
	dev_info(dev,"template_suspend( %p )\n", dev );
	return 0;
}

static int template_resume(struct device *dev)
{
	/* Stromsparmodus ausschalten, Geraet reaktivieren
	 * ...
	 */
	dev_info(dev,"template_resume( %p )\n",dev);
	return 0;
}
#endif

static int __init template_init( void )
{
	if( alloc_chrdev_region(&template_dev_number,0,1,TEMPLATE)<0 )
		return -EIO;
	driver_object = cdev_alloc(); /* Anmeldeobjekt reservieren */
	if( driver_object==NULL )
		goto free_device_number;
	driver_object->owner = THIS_MODULE;
	if( cdev_add(driver_object,template_dev_number,1) )
		goto free_cdev;
	template_class = class_create( THIS_MODULE, TEMPLATE );
	if( IS_ERR( template_class ) ) {
		pr_err("template: no udev support\n");
		goto free_cdev;
	}
#ifdef CONFIG_PM /* Powermanagement */
	template_class->suspend = template_suspend;
	template_class->resume = template_resume;
#endif
	template_dev = device_create( template_class, NULL,
		template_dev_number, NULL, "%s", TEMPLATE );
	return 0;
free_cdev:
	kobject_put( &driver_object->kobj );
free_device_number:
	unregister_chrdev_region( template_dev_number, 1 );
	return -EIO;
}

static void __exit template_exit( void )
{
	device_destroy( template_class, template_dev_number );
	class_destroy( template_class );
	cdev_del( driver_object );
	unregister_chrdev_region( template_dev_number, 1 );
	return;
}

module_init( template_init );
module_exit( template_exit );
MODULE_LICENSE( "GPL" );
