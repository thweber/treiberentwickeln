/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define max(x,y) ((x)<(y)?(y):(x))

int main( int argc, char **argv )
{
	int fd1, fd2, retval;
	fd_set rfds;
	char buffer[10];

	FD_ZERO(&rfds);
	fd1=open("/dev/lift1", O_RDONLY );
	fd2=open("/dev/lift2", O_RDONLY );
	if( (fd1<0) || (fd2<0) ) {
		perror( "mydevice" );
		exit( -1 );
	}
	FD_SET(fd1, &rfds);
	FD_SET(fd2, &rfds);
	retval = select(max(fd1,fd2)+1, &rfds, NULL, NULL, NULL);
	/* Don't rely on the value of tv now! */
	if( FD_ISSET(fd1,&rfds) ) {
		printf("starte lesen fd1 ...\n");
		read( fd1, buffer, sizeof(buffer) );
	}
	if( FD_ISSET(fd2,&rfds) ) {
		printf("starte lesen fd2 ...\n");
		read( fd2, buffer, sizeof(buffer) );
	}
	printf("end\n");
	return 0;
}
