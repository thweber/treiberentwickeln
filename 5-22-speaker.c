/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <asm/uaccess.h>
#include <asm/io.h>

static dev_t speaker_dev_number;
static struct cdev *driver_object;
static struct class *speaker_class;
static struct device *speaker_dev;

static ssize_t driver_write( struct file *instanz, __user const char *user,
	size_t count, loff_t *offs)
{
	u16 tonwert;
	s8 save;
	char buffer[6];

	if( count > sizeof(buffer) )
		count = sizeof(buffer);
	count-=copy_from_user( buffer, user, count );
	buffer[sizeof(buffer)-1] = '\0';
	tonwert = (u16) simple_strtoul( buffer, NULL, 0 );
	dev_info(speaker_dev,"tonwert=%d\n", tonwert );
	if( tonwert ) {
		tonwert = CLOCK_TICK_RATE/tonwert;
		dev_info(speaker_dev,"tonwert=%x\n", tonwert );
		outb( 0xb6, 0x43 );
		outb_p(tonwert & 0xff, 0x42);
		outb((tonwert>>8) & 0xff, 0x42);
		save = inb( 0x61 );
		outb( save | 0x03, 0x61 );
	} else {
		outb(inb_p(0x61) & 0xFC, 0x61);
	}
	return count;
}

static struct file_operations fops = {
	.owner=THIS_MODULE,
	.write=driver_write,
};

static int __init mod_init( void )
{
	if( alloc_chrdev_region(&speaker_dev_number,0,1,"speaker")<0 )
		return -EIO;
	driver_object = cdev_alloc(); /* Anmeldeobjekt reservieren */
	if( driver_object==NULL )
		goto free_device_number;
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if( cdev_add(driver_object,speaker_dev_number,1) )
		goto free_cdev;
	/* Eintrag im Sysfs, damit Udev den Geraetedateieintrag erzeugt. */
	speaker_class = class_create( THIS_MODULE, "speaker" );
	if( IS_ERR( speaker_class ) ) {
		pr_err( "speaker: no udev support available\n");
		goto free_cdev;
	}
	speaker_dev = device_create( speaker_class, NULL, speaker_dev_number,
			NULL, "%s", "speaker" );
	return 0;
free_cdev:
	kobject_put( &driver_object->kobj );
free_device_number:
	unregister_chrdev_region( speaker_dev_number, 1 );
	return -EIO;
}

static void __exit mod_exit( void )
{
	outb(inb_p(0x61) & 0xFC, 0x61);
	/* Loeschen des Sysfs-Eintrags und damit der Geraetedatei */
	device_destroy( speaker_class, speaker_dev_number );
	class_destroy( speaker_class );
	/* Abmelden des Treibers */
	cdev_del( driver_object );
	unregister_chrdev_region( speaker_dev_number, 1 );
	return;
}

module_init( mod_init );
module_exit( mod_exit );
MODULE_LICENSE("GPL");
