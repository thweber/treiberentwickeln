/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/ioport.h>
#include <asm/io.h>

#define MAX_BOARDS 4
static int io[] __initdata = { 0x200, 0x300, 0x400, 0x500, 0 };
static int boardnr=0;

struct _board {
	int io;
	/* und andere Parameter ... */
} board[MAX_BOARDS];

static int __init check_card( int io_port )
{
	u32 magic;

	if( request_region( io_port, 16, "MeineHardware" )==NULL ) {
		magic = inl( io_port );
		if( magic == 0x11223344 ) { /* Hardware gefunden */
			pr_info("Hardware gefunden bei 0x%x!\n", io_port );
			return 0;
		}
		release_region( io_port, 16 );
	}
	return -1;
}

static int __init init_driver(void)
{
	int i;

	for( i=0; io[i]!=0; i++ ) {        /* Fuer alle Adresslagen */
		if( check_card( io[i] )==0 ) { /* Karte gefunden */
			board[boardnr].io=io[i]; /* HW-Parameter abspeichern */
			if( ++boardnr > MAX_BOARDS )
				break;
		}
	}
	if( boardnr == 0 ) {
		return -EIO; /* Keine Karten gefunden. */
	}
	/* ... */
	return 0;
}

static void __exit exit_driver(void)
{
	for( ; boardnr; boardnr-- )
		release_region( board[boardnr].io, 16 );
}

module_init( init_driver );
module_exit( exit_driver );
MODULE_LICENSE("GPL");
