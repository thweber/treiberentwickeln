/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/sched.h>

static void work_queue_function( struct work_struct *data )
{
	pr_info( "work_queue_function( %p, jiffies: %ld ) %d\n",
		data, jiffies, current->pid );
	return;
}

static DECLARE_WORK(work, work_queue_function);

static int __init mod_init(void)
{
	if( schedule_work( &work )==0 ) {
		pr_info( "schedule_work not successful ...\n");
	} else {
		pr_info( "schedule_work successful ...\n");
	}
	return 0;
}

static void __exit mod_exit(void)
{
	pr_info("mod_exit called\n");
	flush_scheduled_work();
}

module_init( mod_init );
module_exit( mod_exit );
MODULE_LICENSE("GPL");
