/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/init.h>

static struct proc_dir_entry *proc_dir, *proc_file;

static int proc_read( char *buf, char **start, off_t offset,
    int size, int *peof, void *data)
{
  int bytes_written=0, ret;

  ret=snprintf(buf+bytes_written,(size-bytes_written),"proc_read wurde\n");
  bytes_written += (ret>(size-bytes_written)) ? (size-bytes_written):ret;
  ret=snprintf(buf+bytes_written, size-bytes_written, "aufgerufen.\n");
  bytes_written += (ret>(size-bytes_written)) ? (size-bytes_written):ret;

  *peof = 1;
  return bytes_written;
}

static int __init proc_init(void)
{
	proc_dir = proc_mkdir( "example", NULL );
	proc_file=create_proc_entry( "example_file", S_IRUGO, proc_dir );
	if( proc_file ) {
		proc_file->read_proc = proc_read;
		proc_file->data = NULL;
	}
	return 0;
}

static void __exit proc_exit(void)
{
	if( proc_file ) remove_proc_entry( "example_file", proc_dir );
	if( proc_dir )  remove_proc_entry( "example", NULL );
}

module_init( proc_init );
module_exit( proc_exit );
MODULE_LICENSE("GPL");
