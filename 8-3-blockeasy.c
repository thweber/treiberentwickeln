/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/init.h>
#include <linux/blkdev.h>

#define SIZE_IN_KBYTES 64

static int major;
static struct gendisk *disk;
static struct request_queue *bdqueue;
static char *mempool;

static struct block_device_operations bops;

static int bd_make_request( struct request_queue *q, struct bio *bio )
{
	char *kaddr, *maddr;
	struct bio_vec *bvec;
	int segnr;

	printk("bd_make_request(%p)\n", bio);
	blk_queue_bounce( q, &bio ); /* higmemory-support */
	bio_for_each_segment( bvec, bio, segnr ) {
		kaddr = bio_data(bio);
		maddr = mempool + (512 * bio->bi_sector);

		if( bio_data_dir( bio )==READ || bio_data_dir( bio )==READA ) {
			printk("read: %p - %p len=%d\n",kaddr,maddr,bio->bi_size);
			memcpy( kaddr, maddr, bio->bi_size );
		} else {
			printk("write: %p - %p len=%d\n",maddr,kaddr,bio->bi_size);
			memcpy( maddr, kaddr, bio->bi_size );
		}
	}
	bio_endio( bio, 0 );
	return 0;
}

static int __init mod_init(void)
{
	printk("mod_init called\n");
	if((major=register_blkdev(0,"bds"))==0) {
		printk("bd: can't get majornumber\n");
		return -EIO;
	}
	if( !(mempool=kmalloc(SIZE_IN_KBYTES*1024,GFP_KERNEL)) ) {
		printk("kmalloc failed ...\n");
		goto out_no_mem;
	}
	bdqueue = blk_alloc_queue( GFP_KERNEL );
	blk_queue_make_request( bdqueue, bd_make_request );
	disk = alloc_disk(1);
	if( !disk ) {
		printk("alloc_disk failed ...\n");
		goto out;
	}
	disk->major = major;
	disk->first_minor = 0;
	disk->fops = &bops;
	disk->queue = bdqueue;
	sprintf(disk->disk_name, "bds0");
	set_capacity( disk, (SIZE_IN_KBYTES * 1024)>>9 ); /* in 512 Byte Bloecke */
	add_disk( disk );
	return 0;
out:
	kfree( mempool );
out_no_mem:
	unregister_blkdev(major, "bds" );
	return -EIO;
}

static void __exit mod_exit(void)
{
	printk("ModExit called\n");
	unregister_blkdev(major, "bds" );
	del_gendisk(disk);
	put_disk( disk );
	blk_cleanup_queue( bdqueue );
	kfree( mempool );
}

module_init( mod_init );
module_exit( mod_exit );
MODULE_LICENSE("GPL");
/* vim: set ts=4 ic aw sw=4: */
