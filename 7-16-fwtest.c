/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/device.h>
#include <linux/firmware.h>

static DECLARE_COMPLETION( obj_not_in_use );
static const struct firmware *fw_entry;

static void firmware_device_release( struct device *dev )
{
    complete( &obj_not_in_use );
}

static struct device fwtest_device = {
    /*.bus_id    = "fwtest", */
    .init_name = "fwtest",
    .release   = firmware_device_release,
};

static void fwtest_load_firmware(const u8 *firmware, int size)
{
    unsigned char buf[80+1];
    int to_copy;

    printk("fwtest_load_firmware(void)\n");
    to_copy = min( (int)(sizeof(buf)-1), size );
    memcpy(buf, firmware, to_copy);
    buf[to_copy] = '\0';
    printk("firmware: \"%s\"\n", buf);
}

static int __init fwtest_init(void)
{
    if( device_register( &fwtest_device ) ) {
        printk("device_register failed\n");
        return -EIO;
    }
    if(request_firmware(&fw_entry,"pseudo_firmware",&fwtest_device)!=0) {
        printk("firmware not available\n");
    	device_unregister( &fwtest_device );
        return -EIO;
    }

    fwtest_load_firmware(fw_entry->data, fw_entry->size);

    release_firmware(fw_entry);
    return 0;
}

static void __exit fwtest_exit(void)
{
    device_unregister( &fwtest_device );
    wait_for_completion( &obj_not_in_use );
    printk("fwtest_exit()\n");
}

module_init(fwtest_init);
module_exit(fwtest_exit);
MODULE_LICENSE("GPL");
