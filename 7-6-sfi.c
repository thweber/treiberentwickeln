/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/seq_file.h>

static void *iterator_start(struct seq_file *m, loff_t *index)
{
	int i;
	struct task_struct *tptr = current;

	for( i=(int)*index; i && tptr!=tptr->parent; i-- )
		tptr = tptr->parent;
	if( tptr==tptr->parent )
		return NULL;
	return (void *)tptr;
}

static void iterator_stop(struct seq_file *m, void *obj_ident)
{
	return;
}

static void *iterator_next(struct seq_file *m, void *obj_ident, loff_t *index)
{
	struct task_struct *tptr = (struct task_struct *)obj_ident;

	if( tptr == tptr->parent ) {
		return NULL;
	}
	(*index)++;
	return (void *)tptr->parent;
}

static int sf_show(struct seq_file *m, void *obj_ident)
{
	struct task_struct *tptr = (struct task_struct *)obj_ident;

	seq_printf(m, "Prozess PID: %d\n", tptr->pid );
	seq_printf(m, "     Eltern: %p\n", tptr->parent );
	return 0;
}

static struct seq_operations sops = {
	.start = iterator_start,
	.next  = iterator_next,
	.stop  = iterator_stop,
	.show  = sf_show,
};

static int proc_open( struct inode *geraete_datei, struct file *instanz )
{
	return seq_open( instanz, &sops );
}

static struct file_operations fops = {
	.owner   = THIS_MODULE,
	.open    = proc_open,
	.read    = seq_read,
	.llseek  = seq_lseek,
	.release = seq_release,
};

static int __init mod_init(void)
{
	static struct proc_dir_entry *procdirentry;

	procdirentry=create_proc_entry( "SequenceFileTest", 0, NULL );
	if( procdirentry ) {
		procdirentry->proc_fops = &fops;
	}
	return 0;
}

static void __exit mod_exit(void)
{
	remove_proc_entry( "SequenceFileTest", NULL );
}

module_init( mod_init );
module_exit( mod_exit );
MODULE_LICENSE("GPL");
