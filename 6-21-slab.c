/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include <linux/delay.h>

static struct task_struct *thread_id;
static DECLARE_COMPLETION( on_exit );
static struct kmem_cache *cache;

struct linobj {
	int dummy_i, dummy_j, dummy_k;
	char dummy_feld[250];
	wait_queue_head_t wq;
};

static void linobj_constructor( void *objp )
{
	struct linobj *ptr = (struct linobj *)objp;

	pr_info("linobj_constructor(%p)\n", objp);
	init_waitqueue_head(&ptr->wq);
	return;
}

static int thread_code( void *data )
{
	unsigned long retvalue;
	int i;
	struct linobj *obj;

	allow_signal( SIGTERM );
	for( i=0; i<5; i++ ) {
		obj = (struct linobj *)kmem_cache_alloc( cache, GFP_KERNEL );
		pr_info("objadr=%p\n", obj );
		retvalue=msleep_interruptible(1000); /* 1 Sekunde */
		kmem_cache_free( cache, obj );
		if( retvalue )
			break;
	}
	complete_and_exit( &on_exit, 0 );
}

static int __init slab_init(void)
{
	cache = kmem_cache_create( "linobj", sizeof(struct linobj),
		0, 0, linobj_constructor );
	if( !cache )
		return -EFAULT;
	thread_id=kthread_run( thread_code, NULL, "linobj" );
	if( thread_id==0 ) {
		kmem_cache_destroy( cache );
		return -EIO;
	}
	return 0;
}

static void __exit slab_exit(void)
{
	kill_pid( task_pid(thread_id), SIGTERM, 1 );
	wait_for_completion( &on_exit );
	if( cache )
		kmem_cache_destroy( cache );
}

module_init( slab_init );
module_exit( slab_exit );
MODULE_LICENSE("GPL");
