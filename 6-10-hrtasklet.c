/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/hrtimer.h>

static struct tasklet_hrtimer fireup;

static enum hrtimer_restart hrtimer_func( struct hrtimer *hrt )
{
	printk(KERN_INFO "hrtimer_func called at %ld...\n", jiffies);
	return HRTIMER_NORESTART;
}

static int __init mod_init(void)
{
	static ktime_t start_in;

	printk(KERN_INFO "mod_init called at %ld\n", jiffies);
	tasklet_hrtimer_init( &fireup, hrtimer_func, CLOCK_REALTIME,
		HRTIMER_MODE_REL );
	start_in = ktime_set( 10, 0 );
	tasklet_hrtimer_start( &fireup, start_in, HRTIMER_MODE_REL );
	return 0;
}

static void __exit mod_exit(void)
{
	printk(KERN_INFO "mod_exit called\n");
	tasklet_hrtimer_cancel( &fireup );
}

module_init( mod_init );
module_exit( mod_exit );

MODULE_LICENSE("GPL");
