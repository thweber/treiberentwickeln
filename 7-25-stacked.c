/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/init.h>

extern void ton_an( u16 tonwert );
extern char *textbuf;

static void (*ton_an_function)(u16 var );
static char **textbufptr;

static int __init driver_init(void)
{
	ton_an_function = symbol_get( ton_an );
	if( ton_an_function )
		ton_an_function( 330 );
	else
		printk("can't find address of symbol \"ton_an\"\n");
	textbufptr = symbol_get( textbuf );
	if( textbufptr )
		printk("content of 0x%p: \"%s\"\n", textbufptr, *textbufptr );
	else
		printk("can't find address of symbol \"textbufptr\"\n");
	return 0;
}

static void __exit driver_exit(void)
{
	if( ton_an_function ) {
		ton_an_function( 0 );
		symbol_put( ton_an );
		//symbol_put_addr(ton_an_function); /* alternativ zu symbol_put */
	}
	if( textbufptr )
		symbol_put( textbuf );
}

module_init( driver_init );
module_exit( driver_exit );

MODULE_LICENSE("GPL");
