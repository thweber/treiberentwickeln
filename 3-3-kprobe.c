/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/kprobes.h>
#include <linux/kallsyms.h>

static int call_count = 0;

static int pre_probe(struct kprobe *p, struct pt_regs *regs)
{
	++call_count;
	printk("pre_probe ...\n");
	return 0;
}

static struct kprobe kp = {
	.pre_handler = pre_probe,
	.post_handler = NULL,
	.fault_handler = NULL,
	.addr = (kprobe_opcode_t *) NULL,
	.symbol_name = "sys_setuid",
};

static int __init probe_init(void)
{
	int ret;

	if( (ret=register_kprobe(&kp))<0 ) {
		printk("could not find address"
			"for the specified symbol name\n");
		return ret;
	}
	printk("kprobe registered address %p\n", kp.addr);
	return 0;
}

static void __exit probe_exit(void)
{
	unregister_kprobe(&kp);
	printk("probe-breakpoint called %d times.\n", call_count);
}

module_init( probe_init );
module_exit( probe_exit );
MODULE_LICENSE("GPL");
