/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/version.h>
#include <linux/timer.h>
#include <linux/sched.h>
#include <linux/init.h>

static struct timer_list mytimer;

static void inc_count(unsigned long arg)
{
	printk("inc_count called (%ld)...\n", mytimer.expires );
	mytimer.expires = jiffies + (2*HZ); /* 2 second */
	add_timer( &mytimer );
}

static int __init ktimer_init(void)
{
	init_timer( &mytimer );
	mytimer.function = inc_count;
	mytimer.data = 0;
	mytimer.expires = jiffies + (2*HZ); /* 2 second */
	add_timer( &mytimer );
	return 0;
}

static void __exit ktimer_exit(void)
{
	if( timer_pending( &mytimer ) )
		printk("Timer ist aktiviert ...\n");
	if( del_timer_sync( &mytimer ) )
		printk("Aktiver Timer deaktiviert\n");
	else
		printk("Kein Timer aktiv\n");
}

module_init( ktimer_init );
module_exit( ktimer_exit );

MODULE_LICENSE("GPL");
