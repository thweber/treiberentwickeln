/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
#include <linux/module.h>
#include <linux/kthread.h>

static wait_queue_head_t wq;
static DECLARE_COMPLETION( on_exit );
static struct rt_mutex tmut;
static struct task_struct *plow, *phigh;

static int process( void *data )
{
	unsigned long timeout;
	struct sched_param schedpar;
	int i;

	schedpar.sched_priority = (long)data;
	sched_setscheduler( current, SCHED_FIFO, &schedpar );
	printk("Thread %d Prio %d: rt_mutex_lock().\n",
		current->pid, current->rt_priority );
	/* CRITICAL SECTION */
	if( rt_mutex_lock_interruptible(&tmut,0)<0 ) {
		printk("Interrupt waehrend rt_mutex_lock_interruptible\n");
	} else {
		printk("Thread %d Prio %d: im kritischen Abschnitt.\n",
			current->pid, current->rt_priority );
		for( i=0; i<5; i++ ) {
			timeout=HZ; /* wait 1 second */
			timeout=wait_event_interruptible_timeout(
				wq, (timeout==0), timeout);
			printk("Thread %d Prio %d: aktuelle Prioritaet: %d\n",
				current->pid, current->rt_priority,
				99-current->prio );
		}
	}
	printk("Thread %d Prio %d: rt_mutex_unlock().\n",
		current->pid, current->rt_priority);
	rt_mutex_unlock( &tmut );
	/* END OF CRITICAL SECTION */
	printk("Thread %d Prio %d: aktuelle Prioritaet: %d\n",
		current->pid, current->rt_priority, 99-current->prio );
	complete_and_exit( &on_exit, 0 );
}

static int __init kthread_init(void)
{
	unsigned long timeout;

	init_waitqueue_head(&wq);
	rt_mutex_init( &tmut );

	printk("Starte Kernel-Thread mit niedriger Prio...\n");
	plow = kthread_create(process, (void *)50, "prio-low" );
	if (IS_ERR(plow)) return -EFAULT;
	wake_up_process( plow );

	timeout=HZ; /* wait 1 second */
	timeout=wait_event_interruptible_timeout(wq,(timeout==0),timeout);

	printk("Starte Kernel-Thread mit hoher Prio...\n");
	phigh = kthread_create(process, (void *)70, "prio-high" );
	if (IS_ERR(phigh)) return -EFAULT;
	wake_up_process( phigh );

	return 0;
}

static void __exit kthread_exit(void)
{
	printk("kthread_exit()\n");
	/* kthread_stop( p ); */
	wait_for_completion( &on_exit );
	wait_for_completion( &on_exit );
}

module_init( kthread_init );
module_exit( kthread_exit );

MODULE_LICENSE("GPL");
