/************************************************************************/
/* Quellcode zum Buch                                                   */
/*                     Linux Treiber entwickeln                         */
/* (3. Auflage) erschienen im dpunkt.verlag                             */
/* Copyright (c) 2004-2011 Juergen Quade und Eva-Katharina Kunst        */
/*                                                                      */
/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* This program is distributed in the hope that it will be useful,      */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         */
/* GNU General Public License for more details.                         */
/*                                                                      */
/************************************************************************/
/***********************************************************************/
/* Geraete-Treiber Template                                            */
/* Das Template unterstuetzt:                                          */
/* - udev (Sysfilesystem)                                              */
/* - Powermanagement                                                   */
/* - Basis-Treiberfunktionen (driver_read, driver_write)               */
/* - Zugriff im BLOCKING-Mode                                          */
/* - Zugriff im NONBLOCKING-Mode                                       */
/* - select/poll                                                       */
/* Es wird eine Bedingung (C-Code) benoetigt, anhand derer entschieden */
/* werden kann, ob Daten zum Lesen bereitstehen beziehungsweise ob     */
/* Daten (in den Kernel, auf die Hardware) geschrieben werden koennen. */
/* Hier im Template wird das ueber die beiden globalen Variablen       */
/* "bytes_available" und "bytes_that_can_be_written" abgewickelt. Die  */
/* Variablen sollen die Anzahl der Bytes enthalten, die aktuell        */
/* gelesen oder geschrieben werden koennen.                            */
/* Die Variable "kernelmem" repraesentiert einen Speicher, in dem sich */
/* die Daten, die die Applikationsinstanz anfordert, befinden. Diese   */
/* Variable wird beispielsweise innerhalb einer ISR gesetzt, in der    */
/* dann auch "bytes_available" angepasst wird. Der Zugriff auf den     */ 
/* Speicher ist ueber Spinlocks zu sichern.                            */
/*                                                                     */
/* Sa 13. Feb 09:59:43 CET 2010, J. Quade, quade@quku.de               */
/***********************************************************************/
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/sched.h>
#include <linux/poll.h>
#include <asm/uaccess.h>
#include <asm/atomic.h>

/* TEMPLATE durch den eigenen Treibernamen ersetzen */
#define TEMPLATE "template"

static dev_t template_dev_number;
static struct cdev *driver_object;
struct class *template_class;
static struct device *template_dev;
static wait_queue_head_t wq_read, wq_write;
static char *Version="$Id: drvtemplate.c,v 1.1 2010/02/13 09:00:09 quade Exp quade $";

static atomic_t bytes_that_can_be_written=ATOMIC_INIT(0); /* anpassen */
static atomic_t bytes_available=ATOMIC_INIT(0);           /* anpassen */
#define READ_POSSIBLE  (atomic_read(&bytes_available)!=0)
#define WRITE_POSSIBLE (atomic_read(&bytes_that_can_be_written)!=0)

static int driver_open( struct inode *devicefile, struct file *instance )
{
	return 0;
}

static int driver_close( struct inode *devicefile, struct file *instance )
{
	return 0;
}

ssize_t driver_read( struct file *instance, char __user *buffer,
	size_t max_bytes_to_read, loff_t *offset)
{
	size_t to_copy, not_copied;
	char kernelmem[128]; /* anpassen */

	/* Daten von der Hardware in den Kernelspace kopieren,
	 * "bytes_available" muss dabei angepasst werden.
	 * Das kann auch "von aussen", beispielsweise durch eine ISR erfolgen.
	 * ACHTUNG: Falls "kernelmem" eine globale Variable wird, ist diese
	 * ueber Spinlocks zu schuetzen.
	 * ...
	 */
	if (!READ_POSSIBLE && (instance->f_flags&O_NONBLOCK) ) {
		/* keine Daten vorhanden und Nonblockingmode */
		return  -EAGAIN;
	}
	if (wait_event_interruptible( wq_read, READ_POSSIBLE ) ) {
		/* waehrend des Schlafens durch ein Signal unterbrochen */
		return -ERESTARTSYS;
	}
	to_copy = min((size_t)atomic_read(&bytes_available),max_bytes_to_read);
	not_copied = copy_to_user( buffer, kernelmem, to_copy );
	atomic_sub( to_copy-not_copied, &bytes_available );
	return to_copy-not_copied;
}

ssize_t driver_write( struct file *instance, const char __user *buffer,
	size_t max_bytes_to_write, loff_t *offset)
{
	size_t to_copy, not_copied;
	char kernelmem[128]; /* Groesse anpassen */

	if (!WRITE_POSSIBLE && (instance->f_flags&O_NONBLOCK) ) {
		/* Hardware/Treiber ist nicht bereit, die Daten zu schreiben
		 * und der Zugriff erfolgt im Nonblockingmode
		 */
		return -EAGAIN;
	}
	if (wait_event_interruptible(wq_write,WRITE_POSSIBLE) ) {
		/* waehrend des Schlafens durch ein Signal unterbrochen */
		return -ERESTARTSYS;
	}
	to_copy = min( (size_t) atomic_read(&bytes_that_can_be_written),
		max_bytes_to_write );
	not_copied = copy_from_user( kernelmem, buffer, to_copy );
	/* Daten koennen jetzt in die Hardware geschrieben werden
	 * ...
	 */
	atomic_sub( to_copy-not_copied, &bytes_that_can_be_written );
	return to_copy-not_copied;
}

unsigned int driver_poll(struct file *instance,
	struct poll_table_struct *event_list)
{
	unsigned int mask=0;

	poll_wait( instance, &wq_read, event_list );
	poll_wait( instance, &wq_write, event_list );
	if (READ_POSSIBLE)
		mask |= POLLIN | POLLRDNORM;
	if (WRITE_POSSIBLE)
		mask |= POLLOUT | POLLWRNORM;
	return mask;
}

static struct file_operations fops = {
	.open = driver_open,
	.release = driver_close,
	.read = driver_read,
	.write = driver_write,
	.poll = driver_poll,
};

#ifdef CONFIG_PM
/* Powermanagement */
static int template_suspend(struct device *dev, pm_message_t state)
{
	switch( state.event ) {
	case PM_EVENT_ON:
		dev_dbg(dev,"on...\n"); break;
	case PM_EVENT_FREEZE:
		dev_dbg(dev,"freeze...\n"); break;
	case PM_EVENT_SUSPEND:
		/* Stromsparmodus einschalten
		 * ...
		 */
		dev_dbg(dev,"suspend...\n"); break;
	case PM_EVENT_HIBERNATE:
		/* Stromsparmodus einschalten
		 * ...
		 */
		dev_dbg(dev,"hibernate...\n"); break;
	default:
		dev_dbg(dev,"pm_event: 0x%x\n", state.event);
		break;
	}
	dev_info(dev,"template_suspend( %p )\n", dev );
	return 0;
}

static int template_resume(struct device *dev)
{
	/* Stromsparmodus ausschalten, Geraet reaktivieren
	 * ...
	 */
	dev_info(dev,"template_resume( %p )\n",dev);
	return 0;
}
#endif

/* Integration des Moduls in den Linux-Kernel */
static int __init template_init( void )
{
	init_waitqueue_head(&wq_read);
	init_waitqueue_head(&wq_write);
	if (alloc_chrdev_region(&template_dev_number,0,1,TEMPLATE)<0)
		return -EIO;
	driver_object = cdev_alloc(); /* Anmeldeobjekt reservieren */
	if (driver_object==NULL)
		goto free_device_number;
	driver_object->owner = THIS_MODULE;
	driver_object->ops = &fops;
	if (cdev_add(driver_object,template_dev_number,1))
		goto free_cdev;
	template_class = class_create( THIS_MODULE, TEMPLATE );
	if (IS_ERR(template_class)) {
		pr_err("template: no udev support\n");
		goto free_cdev;
	}
#ifdef CONFIG_PM
	/* Powermanagement */
	template_class->suspend = template_suspend;
	template_class->resume = template_resume;
#endif
	/* Fuer mehrere Geraetetypen die folgende Zeile mehrfach
         * mit angepasster Minor-Nummer und anderem Namen aufrufen.
         */
	template_dev = device_create( template_class, NULL,
		template_dev_number, NULL, "%s", TEMPLATE );
	dev_info( template_dev, "%s\n", Version );
	return 0;
free_cdev:
	kobject_put( &driver_object->kobj );
free_device_number:
	unregister_chrdev_region( template_dev_number, 1 );
	return -EIO;
}

static void __exit template_exit( void )
{
	/* device_destroy so oft wie device_create aufrufen */
	device_destroy( template_class, template_dev_number );
	class_destroy( template_class );
	cdev_del( driver_object );
	unregister_chrdev_region( template_dev_number, 1 );
	return;
}

module_init( template_init );
module_exit( template_exit );
MODULE_LICENSE( "GPL" );
